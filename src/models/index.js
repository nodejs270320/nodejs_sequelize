// nơi lưu cấu trúc kết nối CSDL
import { Sequelize } from "sequelize";

import config from "../config/config.js";

const { database, host, password, user, port, dialect } = config;

let sequelize = new Sequelize(database, user, password, {
    host: host,
    dialect: dialect    ,
    port: port
});

export default sequelize;
// yarn sequelize -h localhost -d db_food -u root -x 1234 -p 3308 --dialect mysql -o ./src/models -l esm
