import express from "express";
import userRouter from "./userRouter.js";

const rootRouter = express.Router();

rootRouter.use("/user", userRouter);

export default rootRouter;

// localhost:8080/api/user/get-nguoi-dung
// create-nguoi-dung
