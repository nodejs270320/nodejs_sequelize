import {
  signUp,
  login,
  likeRestaurant,
  unLikeRestaurant,
  getRestaurantLikebyUser,
  addComment,
  getListComment,
  createOrder,
} from "../controllers/userController.js";
import express from "express";

const userRouter = express.Router();

import upload from "../controllers/uploadController.js";

userRouter.post("/upload", upload.single("file"), (req, res) => {
  let file = req.file;
  res.send(file);
});

// Định nghĩa file system FS
import fs from "fs";

userRouter.post("/base64", upload.single("file"), (req, res) => {
  // fs.writeFile(process.cwd() + "/test.txt", "Hello World", () => {})
  // let data = fs.readFileSync(process.cwd() + "/test.txt", "utf-8")
  let file = req.file;
  fs.readFile(process.cwd() + "/public/img/" + file.filename, (err, data) => {
    let newName = `data:${file.mimetype};base64,${Buffer.from(data).toString(
      "base64"
    )}`;
    res.send(newName);
  });
});

import { tokenApi } from "../config/jwt.js";

// thêm dữ liệu vào table user => Create
userRouter.post("/signup", signUp);

// kiểm tra dữ liệu trong table user => Read
userRouter.post("/login", login);

//
userRouter.post("/like-restaurant", likeRestaurant);
userRouter.delete("/unlike-restaurant", unLikeRestaurant);
userRouter.get("/get-list-restaurant-liked/:user_id", getRestaurantLikebyUser);
userRouter.post("/add-comment", addComment);
userRouter.get("/get-list-comment/:user_id", getListComment);
userRouter.post("/order", createOrder);

export default userRouter;
