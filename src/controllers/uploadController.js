import multer from "multer";

// __dirname: trả về đường dẫn file đang đứng 
// process.cwd(): trả về đường dẫn gốc
const storage = multer.diskStorage({
    destination: process.cwd() + "/public/img",
    filename: (req, file, callback) => {
        
        let currentTime = new Date();
        let newName = currentTime.getTime() + "_" + file.originalname;
        callback(null, newName)
    }// Đổi tên file trước khi vào req
})

const upload = multer({
    // dest: process.cwd() + "/public/img" //destination: khai báo đường dần lưu tài nguyên ở BE
    storage
})

export default upload;