import { Sequelize } from "sequelize";
import initModels from "../models/init-models.js";
import sequelize from "../models/index.js";
import { error, failCode, successCode } from "../config/response.js";

const models = initModels(sequelize);

// Op
const Op = Sequelize.Op;

import bcrypt from "bcrypt";
import { generateToken } from "../config/jwt.js";

const signUp = async (req, res) => {
  try {
    let { full_name, email, pass_word } = req.body;
    // kiểm tra email có tồn tại hay không
    //
    let checkUser = await models.user.findAll({
      where: {
        email,
      },
    });

    if (checkUser.length > 0) {
      failCode(res, "", "Email đã tồn tại !"); // 400
    } else {
      // yarn add bcrypt

      let newUser = {
        full_name,
        email,
        pass_word: bcrypt.hashSync(pass_word, 10), // mã hóa
      };

      await models.user.create(newUser);
      // send mail
      successCode(res, "", "Đăng ký thành công");
    }
  } catch {
    error(res, "Lỗi BE");
  }
};

const login = async (req, res) => {
  // kiểm tra email và password
  let { email, pass_word } = req.body;

  if (checkUser) {
    // login thành công
    if (bcrypt.compareSync(pass_word, checkUser.pass_word)) {
      checkUser = { ...checkUser, pass_word: "" };

      let token = generateToken(checkUser);
      successCode(res, token, "Login thành công");
    } else {
      failCode(res, "", "Mật khẩu không đúng !");
    }
  } else {
    // email hoặc password không đúng
    failCode(res, "", "Email không đúng !");
  }
};

const likeRestaurant = async (req, res) => {
  try {
    let { user_id, res_id } = req.body;
    let checkUser = await models.user.findOne({
      where: {
        user_id,
      },
    });
    let checkRestaurant = await models.restaurant.findOne({
      where: {
        res_id,
      },
    });
    let checkExits = await models.like_res.findOne({
      where: {
        user_id,
        res_id,
      },
    });
    if (checkExits) {
      failCode(res, "", "Bạn đã like nhà hàng này rồi !");
    }
    if (!checkUser) {
      failCode(res, "", "Không tìm thấy người dùng !");
    } else if (!checkRestaurant) {
      failCode(res, "", "Không tìm thấy nhà hàng !");
    } else {
      let date_like = new Date();
      let newData = {
        user_id,
        res_id,
        date_like,
      };

      await models.like_res.create(newData);
      successCode(res, newData, "Like nhà hàng thành công");
    }
  } catch {
    error(res, "Lỗi BE");
  }
};
const unLikeRestaurant = async (req, res) => {
  try {
    let { user_id, res_id } = req.body;
    await models.like_res.destroy({
      where: {
        user_id,
        res_id,
      },
    });
    successCode(res, "", "Unlike nhà hàng thành công");
  } catch {
    error(res, "Lỗi BE");
  }
};
const getRestaurantLikebyUser = async (req, res) => {
  let { user_id } = req.ss;
  let checkUser = await models.user.findAll({ where: { user_id } });
  if (checkUser) {
    let data = await models.like_res.findAll({
      where: { user_id },
    });

    successCode(res, data, "Lấy danh sách thành công");
  } else {
    failCode(res, "", "Không tìm thấy người dùng !");
  }
};
const addComment = async (req, res) => {
  let { user_id, res_id, amount } = req.body;
  let checkUser = await models.user.findOne({
    where: {
      user_id,
    },
  });
  let checkRestaurant = await models.restaurant.findOne({
    where: {
      res_id,
    },
  });
  if (!checkUser) {
    failCode(res, "", "Không tìm thấy người dùng !");
  } else if (!checkRestaurant) {
    failCode(res, "", "Không tìm thấy nhà hàng !");
  } else {
    let date_rate = new Date();
    let newData = {
      user_id,
      res_id,
      amount,
      date_rate,
    };

    await models.rate_res.create(newData);
    successCode(res, newData, "thêm đánh giá thành công");
  }
};
const getListComment = async (req, res) => {
  let { user_id } = req.params;
  let checkUser = await models.user.findOne({
    where: {
      user_id,
    },
  });
  if (checkUser) {
    let data = await models.rate_res.findAll({
      where: {
        user_id,
      },
    });
    successCode(res, data, "Lấy danh sách thành công");
  } else {
    failCode(res, "", "Không tìm thấy người dùng !");
  }
};

const createOrder = async (req, res) => {
  let { user_id, food_id, amount, code, arr_sub_id } = req.body;
  let dataOrder = {
    user_id,
    food_id,
    amount,
    code,
    arr_sub_id,
  };
  let checkFood = await models.food.findAll({ where: { food_id } });
  let checkUser = await models.user.findOne({
    where: {
      user_id,
    },
  });
  if (!checkFood) {
    failCode(res, "", "Không tìm thấy món ăn !");
  }
  if (!checkUser) {
    failCode(res, "", "Không tìm thấy người dùng !");
  }
  for (let index = 0; index < arr_sub_id.length; index++) {
    let checkSubFood = await models.sub_food.findAll({
      where: { sub_id: arr_sub_id[index], food_id },
    });
    if (!checkSubFood) {
      failCode(res, "", "Không tìm thấy món ăn kèm !");
      return;
    }
  }
  await models.order.create(dataOrder);
  successCode(res, dataOrder, "Đặt món thành công");
};

export {
  signUp,
  login,
  likeRestaurant,
  unLikeRestaurant,
  getRestaurantLikebyUser,
  addComment,
  getListComment,
  createOrder,
};
