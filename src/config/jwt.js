import jwt from "jsonwebtoken";
import { failCode } from "./response.js";
// tạo token
const generateToken = (data) => {
  // 1: dữ liệu muốn mã hóa thành token => payload
  // 2: secret key => signature
  // 3: hạn sử dụng, thuật toán => header

  return jwt.sign({ data }, "VERIFY", { expiresIn: "5m" });
};

// kiểm tra tính hợp lệ của token
const checkToken = (token) => {
  return jwt.verify(token, "VERIFY");
};
// giải mã token
const decodeToken = (token) => {
  return jwt.decode(token);
};

const tokenApi = (req, res, next) => {
  try {
    let { token } = req.headers;

    // kiểm tra token
    if (checkToken(token)) {
      next();
      // đúng next
    } else {
      failCode(res, "", "Bạn cần đăng nhập để thực hiện chức năng này");
    }
  } catch (error) {
    // sai thì báo
    res.status(401).send(error.message);
  }
};

export { tokenApi, generateToken, checkToken, decodeToken };
